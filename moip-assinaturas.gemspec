# -*- encoding: utf-8 -*-
# stub: moip-assinaturas 0.6.5 ruby lib

Gem::Specification.new do |s|
  s.name = "moip-assinaturas".freeze
  s.version = "0.6.5"

  s.required_rubygems_version = Gem::Requirement.new(">= 0".freeze) if s.respond_to? :required_rubygems_version=
  s.require_paths = ["lib".freeze]
  s.authors = ["Warlley".freeze]
  s.date = "2020-11-23"
  s.description = "Ruby Gem para uso do servi\u00E7o de assinaturas do Moip".freeze
  s.email = ["warlleyrezende@gmail.com".freeze]
  s.files = [".gitignore".freeze, "Gemfile".freeze, "Guardfile".freeze, "LICENSE.txt".freeze, "README.md".freeze, "Rakefile".freeze, "lib/moip-assinaturas.rb".freeze, "lib/moip-assinaturas/client.rb".freeze, "lib/moip-assinaturas/coupon.rb".freeze, "lib/moip-assinaturas/customer.rb".freeze, "lib/moip-assinaturas/invoice.rb".freeze, "lib/moip-assinaturas/payment.rb".freeze, "lib/moip-assinaturas/plan.rb".freeze, "lib/moip-assinaturas/subscription.rb".freeze, "lib/moip-assinaturas/version.rb".freeze, "lib/moip-assinaturas/webhooks.rb".freeze, "lib/rails/generators/moip_assinaturas/install_generator.rb".freeze, "lib/rails/generators/templates/moip_assinaturas.rb".freeze, "moip-assinaturas.gemspec".freeze, "spec/fixtures/active_coupon.json".freeze, "spec/fixtures/create_coupon.json".freeze, "spec/fixtures/create_customer.json".freeze, "spec/fixtures/create_plan.json".freeze, "spec/fixtures/create_subscription.json".freeze, "spec/fixtures/custom_authentication/create_coupon.json".freeze, "spec/fixtures/custom_authentication/create_customer.json".freeze, "spec/fixtures/custom_authentication/create_plan.json".freeze, "spec/fixtures/custom_authentication/create_subscription.json".freeze, "spec/fixtures/custom_authentication/details_customer.json".freeze, "spec/fixtures/custom_authentication/details_invoice.json".freeze, "spec/fixtures/custom_authentication/details_payment.json".freeze, "spec/fixtures/custom_authentication/details_plan.json".freeze, "spec/fixtures/custom_authentication/details_subscription.json".freeze, "spec/fixtures/custom_authentication/list_customers.json".freeze, "spec/fixtures/custom_authentication/list_invoices.json".freeze, "spec/fixtures/custom_authentication/list_payments.json".freeze, "spec/fixtures/custom_authentication/list_plans.json".freeze, "spec/fixtures/custom_authentication/list_subscriptions.json".freeze, "spec/fixtures/custom_authentication/update_credit_card.json".freeze, "spec/fixtures/delete_coupon_subscription.json".freeze, "spec/fixtures/details_coupon.json".freeze, "spec/fixtures/details_customer.json".freeze, "spec/fixtures/details_invoice.json".freeze, "spec/fixtures/details_payment.json".freeze, "spec/fixtures/details_plan.json".freeze, "spec/fixtures/details_subscription.json".freeze, "spec/fixtures/generate_bank_slip.json".freeze, "spec/fixtures/inactive_coupon.json".freeze, "spec/fixtures/list_coupons.json".freeze, "spec/fixtures/list_customers.json".freeze, "spec/fixtures/list_invoices.json".freeze, "spec/fixtures/list_payments.json".freeze, "spec/fixtures/list_plans.json".freeze, "spec/fixtures/list_subscriptions.json".freeze, "spec/fixtures/list_subscriptions_queried.json".freeze, "spec/fixtures/update_credit_card.json".freeze, "spec/fixtures/update_customer.json".freeze, "spec/moip-assinaturas/coupon_spec.rb".freeze, "spec/moip-assinaturas/customer_spec.rb".freeze, "spec/moip-assinaturas/invoice_spec.rb".freeze, "spec/moip-assinaturas/payment_spec.rb".freeze, "spec/moip-assinaturas/plan_spec.rb".freeze, "spec/moip-assinaturas/subscription_spec.rb".freeze, "spec/moip-assinaturas/webhooks_spec.rb".freeze, "spec/moip_assinaturas_spec.rb".freeze, "spec/spec_helper.rb".freeze]
  s.homepage = "https://github.com/ibody/moip-assinaturas".freeze
  s.rubygems_version = "3.1.4".freeze
  s.summary = "Ruby Gem para uso do servi\u00E7o de assinaturas do Moip".freeze
  s.test_files = ["spec/fixtures/active_coupon.json".freeze, "spec/fixtures/create_coupon.json".freeze, "spec/fixtures/create_customer.json".freeze, "spec/fixtures/create_plan.json".freeze, "spec/fixtures/create_subscription.json".freeze, "spec/fixtures/custom_authentication/create_coupon.json".freeze, "spec/fixtures/custom_authentication/create_customer.json".freeze, "spec/fixtures/custom_authentication/create_plan.json".freeze, "spec/fixtures/custom_authentication/create_subscription.json".freeze, "spec/fixtures/custom_authentication/details_customer.json".freeze, "spec/fixtures/custom_authentication/details_invoice.json".freeze, "spec/fixtures/custom_authentication/details_payment.json".freeze, "spec/fixtures/custom_authentication/details_plan.json".freeze, "spec/fixtures/custom_authentication/details_subscription.json".freeze, "spec/fixtures/custom_authentication/list_customers.json".freeze, "spec/fixtures/custom_authentication/list_invoices.json".freeze, "spec/fixtures/custom_authentication/list_payments.json".freeze, "spec/fixtures/custom_authentication/list_plans.json".freeze, "spec/fixtures/custom_authentication/list_subscriptions.json".freeze, "spec/fixtures/custom_authentication/update_credit_card.json".freeze, "spec/fixtures/delete_coupon_subscription.json".freeze, "spec/fixtures/details_coupon.json".freeze, "spec/fixtures/details_customer.json".freeze, "spec/fixtures/details_invoice.json".freeze, "spec/fixtures/details_payment.json".freeze, "spec/fixtures/details_plan.json".freeze, "spec/fixtures/details_subscription.json".freeze, "spec/fixtures/generate_bank_slip.json".freeze, "spec/fixtures/inactive_coupon.json".freeze, "spec/fixtures/list_coupons.json".freeze, "spec/fixtures/list_customers.json".freeze, "spec/fixtures/list_invoices.json".freeze, "spec/fixtures/list_payments.json".freeze, "spec/fixtures/list_plans.json".freeze, "spec/fixtures/list_subscriptions.json".freeze, "spec/fixtures/list_subscriptions_queried.json".freeze, "spec/fixtures/update_credit_card.json".freeze, "spec/fixtures/update_customer.json".freeze, "spec/moip-assinaturas/coupon_spec.rb".freeze, "spec/moip-assinaturas/customer_spec.rb".freeze, "spec/moip-assinaturas/invoice_spec.rb".freeze, "spec/moip-assinaturas/payment_spec.rb".freeze, "spec/moip-assinaturas/plan_spec.rb".freeze, "spec/moip-assinaturas/subscription_spec.rb".freeze, "spec/moip-assinaturas/webhooks_spec.rb".freeze, "spec/moip_assinaturas_spec.rb".freeze, "spec/spec_helper.rb".freeze]

  s.installed_by_version = "3.1.4" if s.respond_to? :installed_by_version

  if s.respond_to? :specification_version then
    s.specification_version = 4
  end

  if s.respond_to? :add_runtime_dependency then
    s.add_development_dependency(%q<rake>.freeze, ["~> 10.3.2"])
    s.add_development_dependency(%q<rspec>.freeze, ["~> 2.13"])
    s.add_development_dependency(%q<guard-rspec>.freeze, ["~> 2.5.4"])
    s.add_development_dependency(%q<rspec-its>.freeze, ["~> 1.0.0"])
    s.add_development_dependency(%q<growl>.freeze, ["~> 1.0.3"])
    s.add_development_dependency(%q<fakeweb>.freeze, ["~> 1.3.0"])
    s.add_development_dependency(%q<pry>.freeze, ["~> 0.9"])
    s.add_development_dependency(%q<simplecov>.freeze, ["~> 0.15.0"])
    s.add_runtime_dependency(%q<httparty>.freeze, ["~> 0.15.0"])
    s.add_runtime_dependency(%q<activesupport>.freeze, [">= 2.3.2"])
    s.add_runtime_dependency(%q<json>.freeze, [">= 1.7"])
  else
    s.add_dependency(%q<rake>.freeze, ["~> 10.3.2"])
    s.add_dependency(%q<rspec>.freeze, ["~> 2.13"])
    s.add_dependency(%q<guard-rspec>.freeze, ["~> 2.5.4"])
    s.add_dependency(%q<rspec-its>.freeze, ["~> 1.0.0"])
    s.add_dependency(%q<growl>.freeze, ["~> 1.0.3"])
    s.add_dependency(%q<fakeweb>.freeze, ["~> 1.3.0"])
    s.add_dependency(%q<pry>.freeze, ["~> 0.9"])
    s.add_dependency(%q<simplecov>.freeze, ["~> 0.15.0"])
    s.add_dependency(%q<httparty>.freeze, ["~> 0.15.0"])
    s.add_dependency(%q<activesupport>.freeze, [">= 2.3.2"])
    s.add_dependency(%q<json>.freeze, [">= 1.7"])
  end
end
